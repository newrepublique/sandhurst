﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco;
using umbraco.NodeFactory;
using Umbraco.Core.Logging;
using System.Net.Mail;
using System.Net;
//using EmailHelper;
using System.Configuration;

namespace SandhurstWebSite.Helper
{
    // <summary>
    /// Summary description for CmsConstants
    /// </summary>
    public class CMSConstants
    {
        #region Umbraco Node
        /// <summary>
        /// Defines the node details
        /// </summary>
        public static class Node
        {
            // page node ids
            public static readonly int Home = 1369;
            public static readonly int Navigation = 1513;

            // document node ids
            //public static readonly int Product = 1070;

            // data type ids
            //public static readonly int ProductDataType = ;
        }

        #endregion
    }
    public static class Common
    {

        /// <summary>
        /// Return first node id of maching node name(for all nodes at root level only)
        /// </summary>
        /// <param name="nodeName">node name</param>
        /// <returns></returns>
        public static int GetNodeIdByNodeName(string nodeName)
        {
            try
            {
                // -1 is Node id for root.
                Node node = new Node(-1);
                int nodeId = 0;

                foreach (Node childNode in node.Children)
                {
                    if (string.Equals(childNode.Name, nodeName, StringComparison.OrdinalIgnoreCase))
                    {
                        nodeId = childNode.Id;
                        break;
                    }
                }

                return nodeId;
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
            }
            return 0;
        }

        #region Range methods
        /// <summary>
        /// Get Node for all products for a Range
        /// </summary>
        /// <param name="rangeName"></param>
        /// <returns></returns>
        public static List<Node> GetProdutsForRange(string rangeName)
        {
            try
            {
                var range = uQuery.GetNodesByName(rangeName);
                if (range == null)
                    return null;
                var products = uQuery.GetNodesByType("Product").ToList().FindAll(x => x.GetProperty("productRange") != null && x.GetProperty("productRange").Value.Contains(Convert.ToString(range.FirstOrDefault().Id)));

                return products;
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Get Node for all products for a Range
        /// </summary>
        /// <param name="rangeId">Id of range</param>
        /// <returns>list of products</returns>
        public static List<Node> GetProductsForRangeId(int rangeId, bool isSort = true, string sortOrder = "ASC")
        {
            try
            {
                var products = uQuery.GetNodesByType("Product").ToList().FindAll(x => x.GetProperty("productRange") != null && x.GetProperty("productRange").Value.Contains(Convert.ToString(rangeId)));

                if (products != null && products.Count > 0)
                {
                    if (isSort)
                    {
                        switch (sortOrder.ToUpper())
                        {
                            case "ASC":
                                products = products.OrderBy(x => x.Name).ToList();
                                break;
                            case "DESC":
                                products = products.OrderByDescending(x => x.Name).ToList();
                                break;
                            case "WEIGHT LOW":
                                products = SortProductByWeight(products);
                                break;
                            case "WEIGHT HIGH":
                                products = SortProductByWeight(products, false);
                                break;
                        }
                    }
                }

                return products;
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Get Node for all products
        /// </summary>
        /// <param name="isSort"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public static List<Node> GetAllProducts(bool isSort = true, string sortOrder = "ASC")
        {
            try
            {
                var products = uQuery.GetNodesByType("Product").ToList();

                if (products != null && products.Count > 0)
                {
                    if (isSort)
                    {
                        switch (sortOrder)
                        {
                            case "ASC":
                                products = products.OrderBy(x => x.Name).ToList();
                                break;
                            case "DESC":
                                products = products.OrderByDescending(x => x.Name).ToList();
                                break;
                            case "WEIGHT LOW":
                                products = SortProductByWeight(products);
                                break;
                            case "WEIGHT HIGH":
                                products = SortProductByWeight(products, false);
                                break;
                        }
                    }
                }

                return products;
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Sort products as per qty & unit
        /// </summary>
        /// <param name="products">list of products to be sorted</param>
        /// <param name="asc">order of sorting</param>
        /// <returns>list of sorted products</returns>
        public static List<Node> SortProductByWeight(List<Node> products, bool asc = true)
        {
            List<Node> lstproducts = new List<Node>();
            List<Node> productsWithGram = products.FindAll(x => string.Equals(x.GetProperty<string>("productSizeUnit"), "g", StringComparison.OrdinalIgnoreCase));

            List<Node> productsWithKgOrLtr = new List<Node>();
            productsWithKgOrLtr.AddRange(products);

            productsWithKgOrLtr.RemoveAll(x => string.Equals(x.GetProperty<string>("productSizeUnit"), "g", StringComparison.OrdinalIgnoreCase));

            if (asc)
            {
                productsWithGram = productsWithGram.OrderBy(x => x.GetProperty<string>("productSizeQty")).ToList();
                productsWithKgOrLtr = productsWithKgOrLtr.OrderBy(x => x.GetProperty<string>("productSizeQty")).ThenBy(x => x.GetProperty<string>("productSizeUnit")).ToList();

                lstproducts.AddRange(productsWithGram);
                lstproducts.AddRange(productsWithKgOrLtr);
            }
            else
            {
                productsWithGram = productsWithGram.OrderByDescending(x => x.GetProperty<string>("productSizeQty")).ToList();
                productsWithKgOrLtr = productsWithKgOrLtr.OrderByDescending(x => x.GetProperty<string>("productSizeQty")).ThenBy(x => x.GetProperty<string>("productSizeUnit")).ToList();

                lstproducts.AddRange(productsWithKgOrLtr);
                lstproducts.AddRange(productsWithGram);
            }

            return lstproducts;
        }
        #endregion

        /// <summary>
        /// logic to get image url from id
        /// </summary>
        /// <param name="imageMediaId">id of image</param>
        /// <returns>string value having url of image</returns>
        public static string GetImageUrl(string imageMediaId)
        {
            int imageId;
            string imageUrl = string.Empty;

            try
            {
                if (Int32.TryParse(imageMediaId, out imageId))
                {
                    var media = new umbraco.cms.businesslogic.media.Media(imageId);
                    imageUrl = Convert.ToString(media.getProperty("umbracoFile").Value);
                }

                return imageUrl;
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
                return imageUrl;
            }
        }

        #region Email


        public static void SendEmail(string smtpMailTo, string subjectTitle, string bodyMessage, string smtpMailCC = null, Attachment attachment = null)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient();

                //Load configuration XML file in xml document object

                mail.From = new MailAddress(ConfigurationManager.AppSettings["ContactUsFromMailAddress"]);
                mail.IsBodyHtml = true;
                mail.To.Add(smtpMailTo);
                mail.Subject = subjectTitle;
                mail.Body = bodyMessage;
                SmtpServer.Send(mail);
            }
           
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Sent Email Failed " + ex.StackTrace);
            }
        }

        #endregion

        #region custom Panel Page
        public static string GetPartialViewNameByDocTypeAlias(string docTypename)
        {
            return "~/Views/Partials/" + docTypename + ".cshtml";
        }
        #endregion

        #region newsletter methods
        /// <summary>
        /// Adds subscription email to database
        /// </summary>
        /// <param name="email">Subscription email</param>
        /// <returns>true/false</returns>
        public static bool AddSubscriptionEmail(string email)
        {
            bool result = false;
            try
            {
                using (var sqlHelper = umbraco.BusinessLogic.Application.SqlHelper)
                {
                    var query = @"INSERT INTO newsletterSubscribers (subscriberEmail) VALUES (@Email)";
                    sqlHelper.ExecuteNonQuery(query, sqlHelper.CreateParameter("@Email ", email));
                    result = true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
            }

            return result;
        }

        /// <summary>
        ///  Gets subscribed emails from database and returns as list
        /// </summary>
        /// <returns>list of subscribed email</returns>
        public static List<string> GetSubscriptionEmails()
        {
            List<string> subscribedEmails = null;

            try
            {
                using (var sqlHelper = umbraco.BusinessLogic.Application.SqlHelper)
                {
                    var query = @"SELECT subscriberEmail FROM newsletterSubscribers";
                    var reader = sqlHelper.ExecuteReader(query);

                    if (reader != null)
                    {
                        subscribedEmails = new List<string>();

                        while (reader.Read())
                        {
                            subscribedEmails.Add(reader.GetString("subscriberEmail"));
                        }
                    }

                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
            }

            return subscribedEmails;
        }
        #endregion
    }
}