﻿$(document).ready(function () {
    //Check this is home page
    if ($('#body_prop_typeOfSlide1').length > 0) {
        setHomePageSliderURLPicker();
        for (var i = 1; i <= 5; i++) {
            $("#body_prop_typeOfSlide" + i + "").change(function () {
                setHomePageSliderURLPicker();

            });
        }
    }

    //set title of page automatically
    if ($('#body_prop_pageTitle').length > 0) {
        if ($('#body_prop_pageTitle').val().trim() == "") {
            //Also check span has no error (user is not kept it blank)
            if ($('#body_prop_pageTitle').parent().children('span').length <= 0) {
                $('#body_prop_pageTitle').val($('#body_NameTxt').val());
            }
        }        
    }    
});

//Change content picker/ URL picker based on type of slide selected
function setHomePageSliderURLPicker() {
    for (var i = 1; i <= 5; i++) {
        var typeOfSlide = $('#body_prop_typeOfSlide' + i + ' :selected').text();

        if (typeOfSlide != "" && typeOfSlide == "Featured Campaign") {
            //Click URL picker
            $("#body_prop_cTAButtonLink" + i + "_ctl00").find("li[data-mode='1']").click();
            //Show URL picker
            $("#body_prop_cTAButtonLink" + i + "_ctl00").find("li[data-mode='1']").show();
            //Hide Content Picker
            $("#body_prop_cTAButtonLink" + i + "_ctl00").find("li[data-mode='2']").hide();
        }
        else {
            //Click content picker
            $("#body_prop_cTAButtonLink" + i + "_ctl00").find("li[data-mode='2']").click();
            //Show content picker
            $("#body_prop_cTAButtonLink" + i + "_ctl00").find("li[data-mode='2']").show();
            //Hide URL picker
            $("#body_prop_cTAButtonLink" + i + "_ctl00").find("li[data-mode='1']").hide();
        }
    }
}