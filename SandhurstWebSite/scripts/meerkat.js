// ---------------------- Meerkat.js ---------------------- //
// Powering websites with caffeine and a positive attitude. //
// -------------------------------------------------------- //



$(document).ready(function () {
    // Update the footer year
    var currentYear = (new Date).getFullYear();
    $(".theYear").text(currentYear);
});

// init for flexslider
$('.flexslider').flexslider({
    animation: "slide",
    itemWidth: 240,
    controlNav: false,
    slideshow: false
});

// init for lightcase
$('a[data-rel=lightcase]').lightcase('init', {
    transition: 'fadeInline',
    showSequenceInfo: false
});

// Beware all ye who enter here
// The product filter kraken sits below...
// (WHAT BLACK MAGIC POWERS THIS BEAST?!)
// NOTE: There is additional logic for this filter in foundation.magellan.js, starting at line 81. It involves doing stuff when "position_fixed" is true. //

// Set up variables we'll need..
var filtersAreActive = false, filtersAreOpen = false, numberOfFilters, currentFilter, allFilters, arrowIsUp = false, timesHit = 0;
// start of cybage code full range / Recipe list pages
var startIndex = 0, templateName = $('#templateName').val(), itemListingDivHeight = 0;
var totalProductsCount = 0, productToBeShownCount = 12;
var totalRecipesCount = 0, recipeToBeShownCount = 12;
var rangeProductTypeFilter = [], rangeProductRangeFilter = [], rangeCuisineTypeFilter = [], rangePackagingTypeFilter = [];
var recipeCuisineTypeFilter = [], recipeProductFilter = [], recipeCookingTimeFilter = [], recipeDifficultyFilter = [];

/********** Code to reset checkboxes & dropdown on browser back/forward button click *********/
$('#customDropdown1>option:eq(0)').prop('selected', true);
$('input[type="checkbox"]').prop('checked', false);
$('.checked').removeClass('checked');
$('.activeFilters').empty();
/************** end ***************/

if (templateName == "Range") {
    GetProducts();
}
else if (templateName == "Recipe") {
    GetRecipes();
}

$(window).scroll(function () {
    if ($(this).scrollTop() >= (itemListingDivHeight - 100)) {
        if (templateName == "Range") {
            startIndex = parseInt(startIndex) + parseInt(productToBeShownCount);
            if (totalProductsCount != 0 && startIndex < totalProductsCount) {
                startLoad();
                GetProducts(true);
                loadDone();
            }
            else {
                return;
            }
        }
        else if (templateName == "Recipe") {
            startIndex = parseInt(startIndex) + parseInt(recipeToBeShownCount);
            if (totalRecipesCount != 0 && startIndex < totalRecipesCount) {
                startLoad();
                GetRecipes(true);
                loadDone();
            }
            else {
                return;
            }
        }
    }
});

$('#customDropdown1').change(function () {
    if (templateName == "Range") {
        InitializeRange();
        GetRangeFilter();
        startLoad();
        GetProducts(false);
        loadDone();
    }
    else if (templateName == "Recipe") {
        InitializeRecipe();
        GetRecipeFilter();
        startLoad();
        GetRecipes(false);
        loadDone();
    }
});
// end of cybage code full range / Recipe list pages

// Opens and closes the filter list
$('.filterTrigger a').click(function (e) {

    // Prevent default behavior of link
    e.preventDefault();

    // Hide the active filters panel and reset buttons
    // (consider this part a 'hard reset')
    $('.activeFilters').hide();
    $('.reset').hide();

    // Set the "are filters active?" variable we'll use later.
    // We define filters being active through checking if Foundation's custom "checked" class is visible.
    if ($('.checked:visible').length >= 1) {
        filtersAreActive = true;
    } else {
        filtersAreActive = false;
    };

    // Toggle the filtersAreOpen variable
    // Give variable an existential crisis by setting it to equal not itself (it's a true/false toggle!). Deep stuff. We use this technique again for the arrowIsUp variable, which controls the arrow on the Filter Products dropdown.
    filtersAreOpen = !filtersAreOpen;
    arrowIsUp = !arrowIsUp;

    // Basic check to flip the arrow when panel is open/closed.
    if (arrowIsUp === true) {
        $('.button .icon-angle-down').removeClass('icon-angle-down');
        $('.button i').addClass('icon-angle-up');
    } else {
        $('.button .icon-angle-up').removeClass('icon-angle-up');
        $('.button i').addClass('icon-angle-down');
    };

    // With that out of the way, we open up the container with our filters in it
    $('.filtersInner').slideToggle(150);

    // Hide the active filters (in case they're somehow open)
    $('.activeFilters').hide();
    $('.reset').hide();

    // Since this is TOGGLE based, we'll need to check if the user has clicked to close the dropdown, in which case the active filters should appear. If filters are active, but the panel is shut - that's our cue to show the active filters.
    if (filtersAreActive == true && filtersAreOpen == false) {
        $('.activeFilters').show();
        $('.reset').show();
    };
});

// Right, so that's the dropdown done.
// We also need some logic behind when a checkbox is ticked/unticked:
$('input[type="checkbox"]').change(function (event) {
    // When a checkbox is ticked or unticked, we grab its ID (checkbox1, checkbox2, etc) and set it as a variable. This is so we have access to the most recent checkbox the user interacted with.
    currentFilter = $(this).attr('id');

    // Increase count of timesHit. We increase it once when magellan.js first kicks in (to stop the filters folding away for a user on the first go), and then again here (once the user interacts with the filter). This means the filter won't fold until they've scrolled up-and-down twice, or scrolled down once, seen the filters "click", then applied a filter.
    timesHit = timesHit + 1;

    // We also need to set active filters variable back to false if the user happens to uncheck everything.
    if ($('.checked:visible').length >= 1) {
        filtersAreActive = true;
    } else {
        filtersAreActive = false;
    };

    // Now we reset what we've stored as the currently active filters, then grab all ticked checkboxes and store it in an array. We target parent.parent so that we get a neat bunch of <li> items.
    activeFilters = 0;
    activeFilters = $('.filtersList .checked').parent().parent().toArray();

    // Clear out any left-overs in the active filters section
    $('.activeFilters').empty();

    // Send our array of <li> items into the active filters section
    $(activeFilters).clone().appendTo('.activeFilters');

    // start of cybage code full range / Recipe list pages
    if (templateName == "Range") {
        InitializeRange();
        GetRangeFilter();
        startLoad();
        GetProducts(false);
        loadDone();
    }
    else if (templateName == "Recipe") {
        InitializeRecipe();
        GetRecipeFilter();
        startLoad();
        GetRecipes(false);
        loadDone();
    }
    // end of cybage code full range / Recipe list pages
});

// Actions for the reset button.
$('.reset').click(function (e) {

    e.preventDefault();

    // Uncheck both the custom Foundation tickboxes (remove class), and the hidden 'real' checkboxes
    $('.checked').removeClass('checked');
    //$('input[type="checkbox"]:checked').change();

    // Fade and then empty the active filters
    $('.activeFilters, .reset').fadeOut(100);
    $('.activeFilters').empty();

    // Reset our variables
    activeFilters = 0;
    filtersAreActive = false;

    if (templateName == "Range") {
        InitializeRange();
        GetRangeFilter();
        startLoad();
        GetProducts(false);
        loadDone();
    }
    else if (templateName == "Recipe") {
        InitializeRecipe();
        GetRecipeFilter();
        startLoad();
        GetRecipes(false);
        loadDone();
    }
});

// This is targeting checkboxes inside the active filters section so that unchecking something here updates the original filters as well.
$(document).on("click", ".activeFilters .checkFilters", function () {
    // The data-attr on the <li> items from the array should match the ID of the real, hidden checkboxes.
    // Now we can uncheck the original filter when the 'active filter' is clicked. Since the <li> array is cloned and generated each time an original filter changes, this automatically removes unchecked items from the active filter section.
    currentFilter = $(this).attr('data-attr');

    $('#' + currentFilter).next().removeClass('checked');
    $('#' + currentFilter).change();

    if ($('.checked:visible').length >= 1) {
        filtersAreActive = true;
    } else {
        filtersAreActive = false;
    };

    if (filtersAreActive == false) {
        $('.activeFilters').hide();
        $('.reset').hide();
    };
});

// A 1am bugfix for clicking on the actual tickbox of an active filter not clearing out or doing much of anything... My fix was essentially "RESET EVERYTHING."
$(document).on("click", ".activeFilters .checkbox", function () {
    currentFilter = $(this).parent().parent().attr('data-attr');
    $('#' + currentFilter).next().removeClass('checked');
    $('#' + currentFilter).change();

    if ($('.checked:visible').length >= 1) {
        filtersAreActive = true;
    } else {
        filtersAreActive = false;
    };

    if (filtersAreActive == false) {
        $('.activeFilters').hide();
        $('.reset').hide();
    };

});
// end of product filter logic //

// Note to self - this is the beginnings of the accordian for the filters on mobile devices. This should be mobile specific. (But it's not because #yolo it's 1am)
$('.filtersInner h5').click(function () {
    $(this).next().slideToggle(300);
});

// Logic for the "extras" icons that Brabs forgot to tell me were in the PSDs à² _à² 

//Convert all the icons to an array
var icons = $('.iconItem').toArray();

// Define each column as a group of 3 icons
var column2 = [icons[3], icons[4], icons[5]];
var column3 = [icons[6], icons[7], icons[8]];

// Send the items on their merry way to the correct column
$(column2).appendTo('.column2');
$(column3).appendTo('.column3');

// End of extras icons logic // 

// cybage code full range / Recipe list pages
function InitializeRange() {
    //$("ul.products").html("");
    startIndex = 0;
    totalProductsCount = 0;
}

function GetRangeFilter() {
    rangeProductTypeFilter = [];
    rangeCuisineTypeFilter = [];
    rangeProductRangeFilter = [];
    rangePackagingTypeFilter = [];

    //$('.filtersList input[type="checkbox"]').each(function () {
    $('.activeFilters input[type="checkbox"]').each(function () {
        if ($(this).parent().parent().attr('data-type') == "ProductType") {
            rangeProductTypeFilter.push($(this).attr('id'));
        }
        else if ($(this).parent().parent().attr('data-type') == "CuisineType") {
            rangeCuisineTypeFilter.push($(this).attr('id'));
        }
        else if ($(this).parent().parent().attr('data-type') == "ProductRange") {
            rangeProductRangeFilter.push($(this).attr('id'));
        }
        else if ($(this).parent().parent().attr('data-type') == "PackagingType") {
            rangePackagingTypeFilter.push($(this).attr('id'));
        }
    });

    //console.log('rangeProductTypeFilter = ' + rangeProductTypeFilter);
    //console.log('rangeCuisineTypeFilter = ' + rangeCuisineTypeFilter);
    //console.log('rangeProductRangeFilter = ' + rangeProductRangeFilter);
    //console.log('rangePackagingTypeFilter = ' + rangePackagingTypeFilter);
}

function GetProducts(isScroll) {
    // take sort dropdown value
    var sortValue = $('#customDropdown1').val();

    if (sortValue == 'Name (A-Z)') {
        sortValue = "ASC";
    }
    else if (sortValue == 'Name (Z-A)') {
        sortValue = "DESC";
    }
    else if (sortValue == 'Weight (low to high)') {
        sortValue = "WEIGHT LOW";
    }
    else if (sortValue == 'Weight (high to low)') {
        sortValue = "WEIGHT HIGH";
    }

    //console.log('startIndex = ' + startIndex);

    $.ajax({
        url: '/umbraco/surface/RangeSurface/GetProducts',
        type: "POST",
        data: "{'startIndex':" + startIndex + ", 'itemCount':" + productToBeShownCount + ", 'sortValue':'" + sortValue + "', 'productTypeFilter':" + JSON.stringify(rangeProductTypeFilter) + ", 'productRangeFilter':" + JSON.stringify(rangeProductRangeFilter) + ", 'cuisineTypeFilter':" + JSON.stringify(rangeCuisineTypeFilter) + ", 'packagingTypeFilter':" + JSON.stringify(rangePackagingTypeFilter) + "}",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var products = jQuery.parseJSON(JSON.stringify(data));
            //console.log(products);

            if (!isScroll)
                $("ul.products").html("");

            if (products.length > 0) {
                $.each(products, function (i, o) {
                    if (totalProductsCount == 0) {
                        totalProductsCount = o["ProductCount"];
                        //console.log('totalProductsCount = ' + totalProductsCount);
                    }

                    var element = '<li><a href="{{LINK}}"><img src="{{IMAGE}}.ashx?width=228&height=303&mode=crop&crop=auto&anchor=middlecenter&scale=both" class="productImage" /><h4 class="productTitle">{{TITLE}}</h4><h5 class="subheader productWeight">{{WEIGHT}}</h5></a></li>'
                    element = element.replace("{{LINK}}", o["ProductLink"]);
                    element = element.replace("{{IMAGE}}", o["ProductImage"]);
                    element = element.replace("{{TITLE}}", o["ProductTitle"]);
                    element = element.replace("{{WEIGHT}}", o["ProductSizeQty"]);

                    $("ul.products").append(element);
                });
            }
            else {
                $("ul.products").append('<center><span class="noData">No products found</span></center>');
            }

            itemListingDivHeight = $("ul.products").height();
            //console.log('itemListingDivHeight = ' + itemListingDivHeight);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.statusText);
        }
    });
}

function InitializeRecipe() {
    //$("ul.recipes").html("");
    startIndex = 0;
    totalRecipesCount = 0;
}

function GetRecipeFilter() {
    recipeCuisineTypeFilter = [];
    recipeProductFilter = [];
    recipeCookingTimeFilter = [];
    recipeDifficultyFilter = [];

    $('.activeFilters input[type="checkbox"]').each(function () {
        if ($(this).parent().parent().attr('data-type') == "CuisineType") {
            recipeCuisineTypeFilter.push($(this).attr('id'));
        }
        else if ($(this).parent().parent().attr('data-type') == "Product") {
            recipeProductFilter.push($(this).attr('id'));
        }
        else if ($(this).parent().parent().attr('data-type') == "CookingTime") {
            recipeCookingTimeFilter.push($(this).attr('id'));
        }
        else if ($(this).parent().parent().attr('data-type') == "DifficultyLevel") {
            recipeDifficultyFilter.push($(this).attr('id'));
        }
    });

    //console.log('recipeCuisineTypeFilter = ' + recipeCuisineTypeFilter);
    //console.log('recipeProductFilter = ' + recipeProductFilter);
    //console.log('recipeCookingTimeFilter = ' + recipeCookingTimeFilter);
    //console.log('recipeDifficultyFilter = ' + recipeDifficultyFilter);
}

function GetRecipes(isScroll) {
    // take sort dropdown value
    var sortValue = $('#customDropdown1').val();
    if (sortValue == 'Name (A-Z)') {
        sortValue = "ASC";
    }
    else if (sortValue == 'Name (Z-A)') {
        sortValue = "DESC";
    }

    //console.log('startIndex = ' + startIndex);

    $.ajax({
        url: '/umbraco/surface/RecipeSurface/GetRecipes',
        type: "POST",
        data: "{'startIndex':" + startIndex + ", 'itemCount':" + recipeToBeShownCount + ", 'sortValue':'" + sortValue + "', 'cuisineTypeFilter':" + JSON.stringify(recipeCuisineTypeFilter) + ", 'productFilter':" + JSON.stringify(recipeProductFilter) + ", 'cookingTimeFilter':" + JSON.stringify(recipeCookingTimeFilter) + ", 'difficultyLevelFilter':" + JSON.stringify(recipeDifficultyFilter) + "}",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var recipes = jQuery.parseJSON(JSON.stringify(data));
            //console.log(recipes);

            if (!isScroll) $("ul.recipes").html("");

            if (recipes.length > 0) {
                $.each(recipes, function (i, o) {
                    if (totalRecipesCount == 0) {
                        totalRecipesCount = o["RecipeCount"];
                    }

                    var element = '<li><a href="{{LINK}}"><img src="{{IMAGE}}.ashx?width=227&height=302&mode=crop&crop=auto&anchor=middlecenter&scale=both" class="productImage" /><h4 class="productTitle">{{TITLE}}</h4></a></li>'
                    element = element.replace("{{LINK}}", o["RecipeLink"]);
                    element = element.replace("{{IMAGE}}", o["RecipeImage"]);
                    element = element.replace("{{TITLE}}", o["RecipeTitle"]);

                    $("ul.recipes").append(element);
                });
            }
            else {
                $("ul.recipes").append('<center><span class="noData">No recipes found</span></center>');
            }

            itemListingDivHeight = $("ul.recipes").height();
            //console.log('itemListingDivHeight = ' + itemListingDivHeight);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.statusText);
        }
    });
}
// end of cybage code full range / Recipe list pages

// Start of loading function logic //
function startLoad() {
    $('.filters').prepend('<div class="loading"><img src="/images/loading.gif"></div>');
    $('.loading').fadeIn(300);
    $('.products').addClass('transparent', 300);
    $('.fade').addClass('transparent', 300);
};

function loadDone() {
    $('.products').removeClass('transparent', 300);
    $('.fade').removeClass('transparent', 300);
    $('.loading').fadeOut(300, function () {
        $(this).remove();
    });

};
// end of loading function logic //




// TRACKING MAP CLICKS
var selectedLocation;
// Grab ID of selected state in map
$(document).on("click", 'path', function () {
    selectedLocation = $(this).attr('id');
});

// and for the international map
$('.internationalMap').click(function () {
    selectedLocation = 'international';
});

// Basic jQuery slider for "Copy with Slider" templates/sections
// Need a little bit of trickery up in hurrrr to allow it to work with multiple sliders on the page

// Set up a count
var sliderCount = 0

// for every slider on the page, add the basicSlider[n] class, where n is 
// increased by 1 for each slider
$('.contentSlider').each(function () {
    sliderCount = sliderCount + 1
    $(this).addClass('basicSlider' + sliderCount);
});


// Now look for every element containing 'basicSlider' and call the slider function
$('[class*="basicSlider"]').each(function () {
    $(this).bjqs({
        'height': 'auto',
        'width': 450,
        'responsive': true,
        'animtype': 'slide',
        'automatic': false,
        nexttext: '<i class="icon-angle-right"></i>',
        prevtext: '<i class="icon-angle-left"></i>'
    });
});

// Item with info slider
// Open the item info box when user clicks an item
$('.slides .item:not(".selectedItem")').click(function () {
    var itemNumber;
    // Get number of selected item
    itemNumber = $(this).attr('data-item-num');
    // Hide any item info panels
    $('.itemInfo:visible:not(".selectedItem")').slideUp(350);
    // Remove active state
    $('.selectedItem').removeClass('selectedItem');
    // If there's no visible info panels, show selected item info straight away.
    // Else, wait for a bit for the visible one to close, then show new info.
    if ($('.itemInfo:visible').length == 0) {
        $('.' + itemNumber).slideToggle(350);
    } else {
        setTimeout(function () {
            $('.' + itemNumber).slideToggle(350);
        }, 400);
    }
    // Add active state.
    $(this).addClass('selectedItem');
});

// Close item info if user navigates ahead in the slider
$(document).on("click", ".flex-direction-nav li", function () {
    $('.selectedItem').removeClass('selectedItem');
    $('.itemInfo:visible').slideUp(350);
});

// Close item info if user clicks the close button
$('.closeInfo').click(function () {
    $(this).parent().parent().slideUp(350);
    $('.selectedItem').removeClass('selectedItem');
});

// If you have time, figure out how to make this
// close other dropdowns when one is open
// It's mobile-specific JS that controls the nav accordian thing
enquire.register("screen and (max-width:760px)", function () {
    //console.log('Breakpoint was fired')

    $('a.topLink').click(function () {
        event.preventDefault();
        $(this).next().toggleClass('is-open');
        $(this).find('i').toggleClass('icon-rotate');
    });

    $('.openByDefault').removeClass('is-open');

});

// Obligatory BoC reference
console.log('Howdy to the Dayvan Cowboy.');