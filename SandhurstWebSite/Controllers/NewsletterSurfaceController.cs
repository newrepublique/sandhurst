﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SandhurstWebSite.Models;
using umbraco;
using umbraco.NodeFactory;
using Umbraco.Core.Logging;
using SandhurstWebSite.Helper;
using System.Text.RegularExpressions;

namespace SandhurstWebSite.Controllers
{
    public class NewsletterSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        /// <summary>
        /// Fetches newsletters from cms and returns
        /// </summary>
        /// <param name="startIndex">Index of newsletter item from which next newsletters has to be fetched</param>
        /// <param name="itemCount">Total newsletters to be shown</param>
        /// <returns>list of newsletters</returns>
        [HttpPost]
        public JsonResult GetNewsletters(int startIndex, int itemCount)
        {
            List<Newsletter> newsletters = new List<Newsletter>();

            try
            {
                List<Node> newslettersList = uQuery.GetNodesByType("NewsletterDetail").ToList().OrderByDescending(x => x.CreateDate).Skip(startIndex).Take(itemCount).ToList();

                if (newslettersList != null && newslettersList.Count > 0)
                {
                    foreach (var newsletterItem in newslettersList)
                    {
                        newsletters.Add(new Newsletter
                        {
                            NewsletterTitle = newsletterItem.GetProperty("newsletterTitle").Value,
                            NewsletterDescription = newsletterItem.GetProperty("newsletterDescription").Value,
                            NewsletterFile = newsletterItem.GetProperty("newsletterFile").Value,
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
            }

            return Json(newsletters);
        }

        /// <summary>
        /// Insert email into backend
        /// </summary>
        /// <param name="email">email address which need subscription to newsletter</param>
        /// <returns>string</returns>
        [HttpPost]
        public JsonResult SubscribeNewsLetter(string email)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(email))
            {
                // check if email is in proper format or not
                if (ValidateEmail(email))
                {
                    // Check if email already exist or not
                    var isEmailExist = Common.GetSubscriptionEmails().Contains(email) ? true : false;

                    if (!isEmailExist)
                    {
                        if (Common.AddSubscriptionEmail(email))
                            result = "Email Added";
                        else
                            result = "Error";
                    }
                    else
                        result = "Email Exist";
                }
                else
                    result = "Invalid Email";
            }

            return Json(result);
        }

        /// <summary>
        /// Check if email address is in proper format or not
        /// </summary>
        /// <param name="email">email address to be checked</param>
        /// <returns>true/false</returns>
        public bool ValidateEmail(string email)
        {
            // email address regex
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,4})+)$");
            
            // match email with regex
            Match match = regex.Match(email);

            if (match.Success)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Fetches newsletter subscribed email from backend
        /// </summary>
        /// <returns>list of email as json</returns>
        [HttpPost]
        public JsonResult GetSubscriptionEmails()
        {
            return Json(Common.GetSubscriptionEmails());
        }
    }
}