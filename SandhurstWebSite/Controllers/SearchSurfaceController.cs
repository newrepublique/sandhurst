﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using umbraco.NodeFactory;
using SandhurstWebSite.Models;
using Umbraco.Core.Logging;
using umbraco;
using SandhurstWebSite.Helper;
using Examine;
using System.Text;

namespace SandhurstWebSite.Controllers
{
    public class SearchSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        /// <summary>
        /// logic to search given text in umbraco and fetch results
        /// </summary>
        /// <param name="startIndex">Index from which next results has to be fetched</param>
        /// <param name="itemCount">Total results to be shown</param>
        /// <param name="searchText">text to be searched</param>
        /// <param name="sortValue">sort order of results</param>
        /// <param name="showValue">type of results to be shown</param>
        /// <returns>list of results as jason</returns>
        [HttpPost]
        public JsonResult GetSearchResult(int startIndex, int itemCount, string searchText, string sortValue, string showValue)
        {
            List<Search> searchResult = new List<Search>();
            List<string> dynamicPages = new List<string>();

            try
            {
                // Search 
                var Searcher = ExamineManager.Instance.SearchProviderCollection["ExternalSearcher"];
                var searchCriteria = Searcher.CreateSearchCriteria(Examine.SearchCriteria.BooleanOperation.Or);
                var result = Searcher.Search(searchText, true).ToList();

                if (result != null && result.Count > 0)
                {
                    // list of document types which has to be included in search
                    string[] documentType = new string[] { "RecipeDetails", "Product", "ProductRange", "ContactUs", "CustomContentPage", "GiftBoxes", "Home", "ProductCatalogues", "SandhurstMediaKit", "DistributorsDetail", "NewsletterDetail", "ProductCatalogueDetail", "salesTeamMember", "CopyWithCarouselPanel", "CopyWithImagePanel", "CopyWithQuotePanel", "CopyWithVideoPanel", "DoubleCallToActionPanel", "DoubleColumnCopyPanel", "HeroPanel", "IntroductionPanel", "SingleColumnCopyPanel", "SingleQuotePanel", "StepsPanel", "SuggestionPanel" };

                    // get results as per showvalue
                    if (showValue.ToUpper() == "RECIPES")
                    {
                        // get only recipes pages
                        result = result.Where(x => x.Fields["nodeTypeAlias"].Equals("RecipeDetails")).ToList();
                    }
                    else if (showValue.ToUpper() == "PRODUCTS")
                    {
                        // get only products pages
                        result = result.Where(x => x.Fields["nodeTypeAlias"].Equals("Product")).ToList();
                    }
                    else if (showValue.ToUpper() == "RANGES")
                    {
                        // get only range pages
                        result = result.Where(x => x.Fields["nodeTypeAlias"].Equals("ProductRange")).ToList();
                    }
                    else
                    {
                        // get all pages
                        result = result.Where(x => documentType.Contains(x.Fields["nodeTypeAlias"])).ToList();
                    }

                    if (result != null && result.Count > 0)
                    {
                        Node node = null;
                        int resultCount = 0;

                        // list of dynamic panel document types which has to be matched with searched result
                        string[] dynamicPanelDocumentType = new string[] { "CopyWithCarouselPanel", "CopyWithImagePanel", "CopyWithQuotePanel", "CopyWithVideoPanel", "DoubleCallToActionPanel", "DoubleColumnCopyPanel", "HeroPanel", "IntroductionPanel", "SingleColumnCopyPanel", "SingleQuotePanel", "StepsPanel", "SuggestionPanel" };

                        // get all dynamic pages
                        var lstPages = uQuery.GetNodesByType("CustomContentPage").ToList();

                        bool isDynamicPanel = false;

                        #region Add to list
                        foreach (var item in result)
                        {
                            string description = "Click to view description";

                            // Check for non-paged document type and take actual page url where it will be displayed
                            if (item.Fields["nodeTypeAlias"] == "DistributorsDetail")
                            {
                                node = new Node(Convert.ToInt32(uQuery.GetNodesByType("ourDistributors").FirstOrDefault().Id));
                                isDynamicPanel = false;
                            }
                            else if (item.Fields["nodeTypeAlias"] == "NewsletterDetail")
                            {
                                node = new Node(Convert.ToInt32(uQuery.GetNodesByType("Newsletters").FirstOrDefault().Id));
                                description = item.Fields["newsletterDescription"];
                                isDynamicPanel = false;
                            }
                            else if (item.Fields["nodeTypeAlias"] == "ProductCatalogueDetail")
                            {
                                node = new Node(Convert.ToInt32(uQuery.GetNodesByType("ProductCatalogues").FirstOrDefault().Id));
                                description = item.Fields["catalogueDescription"];
                                isDynamicPanel = false;
                            }
                            else if (item.Fields["nodeTypeAlias"] == "salesTeamMember")
                            {
                                node = new Node(Convert.ToInt32(uQuery.GetNodesByType("ContactUs").FirstOrDefault().Id));
                                description = item.Fields["descriptionBio"];
                                isDynamicPanel = false;
                            }
                            else if (item.Fields["nodeTypeAlias"] == "CustomContentPage")
                            {
                                node = new Node(Convert.ToInt32(item.Id));
                                
                                // check if dynamic pages list already contain the page or not
                                if (dynamicPages != null && dynamicPages.Exists(x => x == node.Name) == false)
                                {
                                    dynamicPages.Add(node.Name);
                                }
                                isDynamicPanel = false;
                            }
                            else if (dynamicPanelDocumentType.Contains(item.Fields["nodeTypeAlias"]))
                            {
                                node = new Node(Convert.ToInt32(item.Fields["parentID"]));

                                // check if dynamic pages list already contain the page which have this panel or not
                                if (dynamicPages != null && dynamicPages.Exists(x => x == node.Name) == false)
                                {
                                    resultCount = resultCount + 1;
                                    searchResult.Add(new Search
                                    {
                                        ResultName = node.Name,
                                        ResultType = "Sandhurst",
                                        ResultDescription = description,
                                        ResultUrl = node.NiceUrl,
                                        ResultCount = 0
                                    });

                                    dynamicPages.Add(node.Name);
                                }

                                isDynamicPanel = true;
                            }
                            else
                            {
                                node = new Node(Convert.ToInt32(item.Fields["id"]));
                                isDynamicPanel = false;

                                if (item.Fields["nodeTypeAlias"] == "ProductRange") description = item.Fields["rangeDescription"];
                                else if (item.Fields["nodeTypeAlias"] == "GiftBoxes") description = item.Fields["bodyCopy"];
                                else if (item.Fields["nodeTypeAlias"] == "RecipeDetails") description = item.Fields["description"];
                                else if (item.Fields["nodeTypeAlias"] == "Product") description = item.Fields["productDescription"];
                                else if (item.Fields["nodeTypeAlias"] == "SandhurstMediaKit") description = item.Fields["mediaKitDescription"];
                            }

                            if (isDynamicPanel == false)
                            {
                                resultCount = resultCount + 1;
                                searchResult.Add(new Search
                                {
                                    ResultName = item.Fields["nodeName"],
                                    ResultType = ResultType(item.Fields["nodeTypeAlias"]),
                                    ResultDescription = description.IndexOf(Environment.NewLine) > 0
                                                        ? description.Substring(0, description.IndexOf(Environment.NewLine))
                                                        : description, // take first para of description field
                                    ResultUrl = node.NiceUrl,
                                    ResultCount = 0
                                });
                            }
                        }
                        #endregion

                        // sort final result as per sortvalue
                        if (sortValue.ToUpper() == "ASC")
                        {
                            searchResult = searchResult.OrderBy(x => x.ResultName).ToList();
                        }
                        else
                        {
                            searchResult = searchResult.OrderByDescending(x => x.ResultName).ToList();
                        }

                        // skip previously shown results & take only required number of results e.g. 4 results
                        searchResult = searchResult.Skip(startIndex).Take(itemCount).ToList();
                        // take count of results
                        searchResult[0].ResultCount = resultCount;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Json(searchResult);
        }

        /// <summary>
        /// logic to return result type as per document type
        /// </summary>
        /// <param name="nodeTypeAlias">document type of page</param>
        /// <returns>type of result as string</returns>
        public string ResultType(string nodeTypeAlias)
        {
            if (nodeTypeAlias == "RecipeDetails") return "Recipe";
            else if (nodeTypeAlias == "Product") return "Product";
            else if (nodeTypeAlias == "ProductRange") return "Range";
            else if (nodeTypeAlias == "ContactUs") return "Contact Us";
            else if (nodeTypeAlias == "CustomContentPage") return "Sandhurst";
            else if (nodeTypeAlias == "GiftBoxes") return "Gift Boxes";
            else if (nodeTypeAlias == "Home") return "Sandhurst";
            else if (nodeTypeAlias == "ProductCatalogues" || nodeTypeAlias == "ProductCatalogueDetail") return "Product Catalogue";
            else if (nodeTypeAlias == "SandhurstMediaKit") return "Sandhurst media Kit";
            else if (nodeTypeAlias == "DistributorsDetail") return "Distributor";
            else if (nodeTypeAlias == "NewsletterDetail") return "Newsletter";
            else if (nodeTypeAlias == "salesTeamMember") return "Sales Team Member";
            else return string.Empty;
        }
    }
}
