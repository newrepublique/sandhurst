﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Security;
using System.Security.Permissions;
using Umbraco.Core.Logging;

namespace SandhurstWebSite.Controllers
{
    public class CaptchaSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        //
        // GET: /CaptchaSurface/

        public ActionResult Index()
        {
            return View();
        }

        //in your controller.cs 
        [HttpPost]
        public JsonResult CaptchaImageContactUs(bool noisy = true)
        {
            return CaptchaImage("Captcha_ContactUs", noisy);
        }

        public JsonResult CaptchaImage(string prefix, bool noisy = true)
        {
            try
            {
                string randomWord = GetRandomWord();

                var rand = new Random((int)DateTime.Now.Ticks);
                //generate new question 
                //int a = rand.Next(10, 99); 
                //int b = rand.Next(0, 9);
                var captcha = randomWord;//string.Format("{0} + {1} = ?", a, b); 

                //store answer 
                Session[prefix] = randomWord; // a + b; 

                //image stream 
                FileContentResult img = null;

                using (var mem = new MemoryStream())
                using (var bmp = new Bitmap(130, 30))
                using (var gfx = Graphics.FromImage((Image)bmp))
                {
                    gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                    gfx.SmoothingMode = SmoothingMode.AntiAlias;
                    gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));

                    //add noise 
                    if (noisy)
                    {
                        int i, r, x, y;
                        var pen = new Pen(Color.Yellow);
                        for (i = 1; i < 10; i++)
                        {
                            pen.Color = Color.FromArgb(
                            (rand.Next(0, 255)),
                            (rand.Next(0, 255)),
                            (rand.Next(0, 255)));

                            r = rand.Next(0, (130 / 3));
                            x = rand.Next(0, 130);
                            y = rand.Next(0, 30);

                            gfx.DrawEllipse(pen, x - r, y - r, r, r);
                        }
                    }

                    //add question 
                    gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.Gray, 2, 3);

                    string strFileName = Guid.NewGuid() + ".jpg";
                    string returnFilePath = "/images/CaptchaImages/" + strFileName;
                    string strFilePath = string.Format(@"{0}{1}", Server.MapPath(@"~\images\CaptchaImages\"), strFileName);

                    if (CheckWritePermissions(Server.MapPath(@"~\images\CaptchaImages\")))
                    {
                        bmp.Save(strFilePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                        DeleteOlderImage();
                        return Json(returnFilePath);
                    }
                    //render as Jpeg 
                    //bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //img = this.File(mem.GetBuffer(), "image/jpg"); 

                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
            }
            return Json(string.Empty);
            //return Json("vaibhav");
        }

        private void DeleteOlderImage()
        {
            try
            {
                string[] files = Directory.GetFiles(Server.MapPath(@"~\images\CaptchaImages\"));
                foreach (string file in files)
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.LastAccessTime < DateTime.Now.AddMinutes(-22))
                        try
                        {
                            fi.Delete();
                        }
                        catch (IOException ex)
                        {

                        }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
            }
        }

        private string GetRandomWord()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            string result = new string(
                Enumerable.Repeat(chars, 6)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }

        /// <summary>
        /// This method is used to check whether directory has permissions.
        /// </summary>
        /// <param name="directoryPath"></param>
        /// <returns></returns>
        public static bool CheckWritePermissions(string filePath)
        {
            bool isWriteAccess = false;
            try
            {
                var permissionSet = new PermissionSet(PermissionState.None);
                var writePermission = new FileIOPermission(FileIOPermissionAccess.AllAccess, filePath);

                permissionSet.AddPermission(writePermission);

                if (permissionSet.IsSubsetOf(AppDomain.CurrentDomain.PermissionSet))
                {
                    isWriteAccess = true;
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                isWriteAccess = false;
            }
            catch (Exception ex)
            {
                isWriteAccess = false;
            }

            return isWriteAccess;
        }
    }
}
