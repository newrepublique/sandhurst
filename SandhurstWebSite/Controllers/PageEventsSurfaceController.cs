﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using umbraco;
using Umbraco.Core.Logging;
using SandhurstWebSite.Helper;
using System.Net;
using System.IO;
using System.Text;
using SandhurstWebSite.Models;


namespace SandhurstWebSite.Controllers
{
    public class PageEventsSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        //
        // GET: /PageEventsSurface/

        public ActionResult Index()
        {
            return View();
        }

        #region Product Catalogs Page
        /// <summary>
        /// Return Product Catalogs for Lazy loading of Product Catalog Page
        /// </summary>
        /// <param name="startIndex"></param>
        /// <returns></returns>

        [HttpPost]
        public JsonResult getProductCatalog(int startIndex)
        {
            List<ProductCatalog> lstProductCatalog = new List<ProductCatalog>();
            try
            {
                List<umbraco.NodeFactory.Node> catalogs = uQuery.GetNodesByType("ProductCatalogueDetail").ToList().OrderByDescending(obj => obj.CreateDate).Skip(startIndex).Take(2).ToList();

                foreach (var catalog in catalogs)
                {
                    ProductCatalog productCatalog = new ProductCatalog();
                    productCatalog.catalogName = catalog.GetProperty("catalogueName").Value;
                    productCatalog.catalogDescription = catalog.GetProperty("catalogueDescription").Value;
                    productCatalog.catalogImagePath = catalog.GetProperty("catalogueImage").Value;
                    productCatalog.catalogFilePath = catalog.GetProperty("catalogueFile").Value;

                    lstProductCatalog.Add(productCatalog);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
            }
            return Json(lstProductCatalog);
        }
        #endregion


        #region Contact Us Page
        /// <summary>
        /// Event for contact Us Form submit button click
        /// </summary>
        /// <param name="objForm"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult contactUsBtnSubmit_Clicked(ContactUsEnqueryForm objForm)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["EnableCaptchaValidation"]))
            {
                if (!ValidateCaptcha(objForm))
                {
                    return Json("InvalidCaptcha");
                }
            }
            if (!ValidateContactUsForm(objForm))
            {
                return Json("false");
            }

            string userIP = Request.UserHostAddress;

            if (submitEnquiryDetails(objForm))
            {
                return Json("true");
            }
            return Json("false");
        }

        /// <summary>
        /// Validate the Contact Us Enquiry Form
        /// </summary>
        /// <param name="objForm"></param>
        /// <returns></returns>
        private bool ValidateContactUsForm(ContactUsEnqueryForm objForm)
        {
            try
            {
                //Check if all mandotory fields are not null or empty or invalid
                if (string.IsNullOrEmpty(objForm.strCustomerType.Trim()))
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(objForm.strEmail.Trim()))
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(objForm.strEnquryType.Trim()))
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(objForm.strname.Trim()))
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(objForm.strPhone.Trim()))
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
                return false;
            }

            return true;
        }

        private bool submitEnquiryDetails(ContactUsEnqueryForm objForm)
        {
            try
            {
                #region Log Data to Log file

                string strData = "\n\n****************Contact Us Form Submission***************";
                strData += "\nCustomer Type: " + objForm.strCustomerType;
                strData += "\nEnqury Type: " + objForm.strEnquryType;
                strData += "\nName" + objForm.strname;
                strData += "\nPhone: " + objForm.strPhone;
                strData += "\nEmail: " + objForm.strEmail;
                strData += "\nEnquiry Details: " + objForm.strEnquiryDetails;
                strData += "\n****************Contact Us Form End***************\n";

                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, strData);

                #endregion

                #region Send Email

                string strEmailSubject = objForm.strEnquryType + " Enquiry from " + objForm.strname;
                string strEmailBody = "<br /> Name: " + objForm.strname + "<br /> Email: " + objForm.strEmail + "<br /> Phone: " + objForm.strPhone
                    + "<br /> Customer Type: " + objForm.strCustomerType + "<br /> Enquiry: " + objForm.strEnquiryDetails;

                SandhurstWebSite.Helper.Common.SendEmail(ConfigurationManager.AppSettings["ContactUsToMailAddress"], strEmailSubject, strEmailBody);

                #endregion

            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
                return false;
            }
            return true;
        }

        /// <summary>
        /// validate captch sent by user
        /// </summary>
        /// <param name="objForm"></param>
        /// <returns></returns>
        private bool ValidateCaptcha(ContactUsEnqueryForm objForm)
        {

            if (Session["Captcha_ContactUs"] != null && Convert.ToString(Session["Captcha_ContactUs"]).Equals(objForm.recaptcha_response_field))
            {
                return true;
            }
            return false;

            #region Removed
            /*
            const string VerifyUrl = "http://api-verify.recaptcha.net/verify";


            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(VerifyUrl);
            req.ProtocolVersion = HttpVersion.Version10;
            req.Timeout = 30 * 1000 
            req.Method = "POST";
            req.UserAgent = "reCAPTCHA/ASP.NET";
            req.AllowWriteStreamBuffering = false;


            req.ContentType = "application/x-www-form-urlencoded";

            string formdata = String.Format(
                "privatekey={0}&remoteip={1}&challenge={2}&response={3}",
                                    HttpUtility.UrlEncode(ConfigurationManager.AppSettings["ReCaptchaPrivateKey"]),
                                    HttpUtility.UrlEncode("172.27.150.65"),
                                    HttpUtility.UrlEncode(objForm.recaptcha_challenge_field),
                                    HttpUtility.UrlEncode(objForm.recaptcha_response_field));

            byte[] formbytes = System.Text.Encoding.ASCII.GetBytes(formdata);

            req.ContentLength = formbytes.Length;

            using (Stream requestStream = req.GetRequestStream())
            {
                requestStream.Write(formbytes, 0, formbytes.Length);
            }

            string[] results;

            try
            {
                using (WebResponse httpResponse = req.GetResponse())
                {
                    using (TextReader readStream = new StreamReader(httpResponse.GetResponseStream(), Encoding.UTF8))
                    {
                        results = readStream.ReadToEnd().Split(new string[] { "\n", "\\n" }, StringSplitOptions.RemoveEmptyEntries);
                    }
                }
            }
            catch (WebException ex)
            {
                //EventLog.WriteEntry("Application", ex.Message, EventLogEntryType.Error);
                return false;
            }

            switch (results[0])
            {
                case "true":
                    return true;
                case "false":
                    string error = results[1].Trim(new char[] { '\'' });
                    return false;
                default:
                    return false;
            }
            */
            #endregion

        }
    }
        #endregion

   
}
