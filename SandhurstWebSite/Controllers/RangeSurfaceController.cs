﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using umbraco.NodeFactory;
using SandhurstWebSite.Models;
using Umbraco.Core.Logging;
using umbraco;
using SandhurstWebSite.Helper;

namespace SandhurstWebSite.Controllers
{
    public class RangeSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        /// <summary>
        /// logic to fetch current range products
        /// </summary>
        /// <param name="startIndex">Index of product item from which next products has to be fetched</param>
        /// <param name="itemCount">Total products to be shown</param>
        /// <returns>list of products as jason</returns>
        [HttpPost]
        public JsonResult GetRangeProductList(int startIndex, int itemCount, int pageId = 0)
        {
            List<Product> products = new List<Product>();

            try
            {
                // get all products of the range & skip previously shown products & take only required number of products e.g. 4 products
                var productList = Common.GetProductsForRangeId(pageId).Skip(startIndex).Take(itemCount).ToList();

                // add products to list
                if (productList != null && productList.Count > 0)
                {
                    foreach (var productItem in productList)
                    {
                        string imageUrl = string.Empty;
                        if (string.IsNullOrEmpty(productItem.GetProperty("productImage").Value))
                        {
                            imageUrl = "http://placehold.it/300x400&text=No+Image";
                        }
                        else
                        {
                            imageUrl = productItem.GetProperty("productImage").Value;
                        }

                        products.Add(new Product
                        {
                            ProductTitle = productItem.Name,
                            ProductImage = imageUrl,
                            ProductSizeQty = productItem.GetProperty("productSizeQty").Value + productItem.GetProperty("productSizeUnit").Value,
                            ProductLink = productItem.NiceUrl
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
            }

            return Json(products);
        }

        /// <summary>
        /// logic to fetch products
        /// </summary>
        /// <param name="startIndex">Index of product item from which next products has to be fetched</param>
        /// <param name="itemCount">Total products to be shown</param>
        /// <param name="sortValue">Sort order in which products to be sorted</param>
        /// <param name="productTypeFilter">product type filter values as per which products had to be fetched</param>
        /// <param name="cuisineTypeFilter">cuisine type filter values as per which products had to be fetched</param>
        /// <param name="packagingTypeFilter">packaging type filter values as per which products had to be fetched</param>
        /// <param name="productRangeFilter">product range filter values as per which products had to be fetched</param>
        /// <returns>list of products as jason</returns>
        [HttpPost]
        public JsonResult GetProducts(int startIndex, int itemCount, string sortValue, List<string> productTypeFilter, List<string> productRangeFilter, List<string> cuisineTypeFilter, List<string> packagingTypeFilter)
        {
            List<Product> products = new List<Product>();

            try
            {
                // get all products & sort them as per sortvalue
                var productList = SandhurstWebSite.Helper.Common.GetAllProducts(true, sortValue);

                // filter all products as per filtervalue
                if (productTypeFilter != null && productTypeFilter.Count > 0)
                    productList = productList.Where(r => r.GetProperty("productType").Value.Split(',').Any(code => productTypeFilter.Contains(code))).ToList();

                if (cuisineTypeFilter != null && cuisineTypeFilter.Count > 0)
                    productList = productList.Where(r => r.GetProperty("cuisineType").Value.Split(',').Any(code => cuisineTypeFilter.Contains(code))).ToList();

                if (productRangeFilter != null && productRangeFilter.Count > 0)
                    productList = productList.Where(r => r.GetProperty("productRange").Value.Split(',').Any(code => productRangeFilter.Contains(code))).ToList();

                if (packagingTypeFilter != null && packagingTypeFilter.Count > 0)
                    productList = productList.Where(r => r.GetProperty("packagingType").Value.Split(',').Any(code => packagingTypeFilter.Contains(code))).ToList();

                // take count of filtered products
                int productCount = productList.Count;

                // skip previously shown products & take only required number of products e.g. 4 products
                productList = productList.Skip(startIndex).Take(itemCount).ToList();

                // add products to list
                if (productList != null && productList.Count > 0)
                {
                    foreach (var productItem in productList)
                    {
                        string imageUrl = string.Empty;
                        if (string.IsNullOrEmpty(productItem.GetProperty("productImage").Value))
                        {
                            imageUrl = "http://placehold.it/300x400&text=No+Image";
                        }
                        else
                        {
                            imageUrl = productItem.GetProperty("productImage").Value;
                        }

                        products.Add(new Product
                        {
                            ProductTitle = productItem.Name,
                            ProductImage = imageUrl,
                            ProductSizeQty = productItem.GetProperty("productSizeQty").Value + productItem.GetProperty("productSizeUnit").Value,
                            ProductLink = productItem.NiceUrl,
                            ProductCount = productCount
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
            }

            return Json(products);
        }
    }
}