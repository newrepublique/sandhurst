﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using umbraco.NodeFactory;
using SandhurstWebSite.Models;
using Umbraco.Core.Logging;
using umbraco;

namespace SandhurstWebSite.Controllers
{
    public class OurDistributorsSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        /// <summary>
        /// logic to fetch locationwise distributors & sort them as per sort value
        /// </summary>
        /// <param name="location">region selected in map</param>
        /// <param name="sort">value of sort dropdown</param>
        /// <returns>json</returns>
        [HttpPost]
        public JsonResult GetDistributorList(string location, string sort)
        {
            List<Distributors> distributors = new List<Distributors>();
            try
            {
                List<Node> distributorsList = uQuery.GetNodesByType("DistributorsDetail").ToList().Where(x => x.GetProperty("location").Value.Substring(0, x.GetProperty("location").Value.IndexOf('-') - 1).Trim().ToUpperInvariant().Equals(location.ToUpperInvariant())).ToList();

                // get full name of location

                var dataTypeId = umbraco.cms.businesslogic.datatype.DataTypeDefinition.GetAll().First(dtDef => dtDef.Text == "Distributor Location Dropdown").Id;
                var locationFullName = uQuery.GetPreValues(dataTypeId).First(x => x.Value.Substring(0, x.Value.IndexOf('-') - 1).Trim().ToUpperInvariant().Equals(location.ToUpperInvariant())).Value;

                if (sort.Equals("Name (A-Z)"))
                    distributorsList = distributorsList.OrderBy(x => x.GetProperty("name").Value).ToList();
                else
                    distributorsList = distributorsList.OrderByDescending(x => x.GetProperty("name").Value).ToList();

                if (distributorsList != null && distributorsList.Count > 0)
                {
                    foreach (var distributorItem in distributorsList)
                    {
                        distributors.Add(new Distributors
                        {
                            PkDistributorId = distributorItem.Id,
                            Name = distributorItem.GetProperty("name").Value,
                            PhoneNumber = distributorItem.GetProperty("phoneNumber").Value,
                            Email = distributorItem.GetProperty("emailAddress").Value,
                            Website = distributorItem.GetProperty("website").Value,
                            Location = distributorItem.GetProperty("location").Value,
                            LocationFullName = locationFullName.Remove(0, locationFullName.IndexOf('-') + 1).Trim()
                        });
                    }
                }
                else
                {
                    distributors.Add(new Distributors
                    {
                        PkDistributorId = 0,
                        Name = null,
                        PhoneNumber = null,
                        Email = null,
                        Website = null,
                        Location = location,
                        LocationFullName = locationFullName.Remove(0, locationFullName.IndexOf('-') + 1).Trim()
                    });
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
            }

            return Json(distributors);
        }
    }
}