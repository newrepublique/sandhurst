﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using umbraco.NodeFactory;
using SandhurstWebSite.Models;
using Umbraco.Core.Logging;
using umbraco;
using SandhurstWebSite.Helper;

namespace SandhurstWebSite.Controllers
{
    public class RecipeSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {
        /// <summary>
        /// logic to fetch recipes
        /// </summary>
        /// <param name="startIndex">Index of recipe item from which next recipes has to be fetched</param>
        /// <param name="itemCount">Total recipes to be shown</param>
        /// <param name="sortValue">Sort order in which recipes to be sorted</param>
        /// <param name="cuisineTypeFilter">cuisine type filter values as per which recipes had to be fetched</param>
        /// <param name="productFilter">product filter values as per which recipes had to be fetched</param>
        /// <param name="cookingTimeFilter">cooking time filter values as per which recipes had to be fetched</param>
        /// <param name="difficultyLevelFilter">difficulty level filter values as per which recipes had to be fetched</param>
        /// <returns>list of recipes as jason</returns>
        [HttpPost]
        public JsonResult GetRecipes(int startIndex, int itemCount, string sortValue, List<string> cuisineTypeFilter, List<string> productFilter, List<string> cookingTimeFilter, List<string> difficultyLevelFilter)
        {
            List<Recipe> recipes = new List<Recipe>();

            try
            {
                // get all recipes
                var recipeList = uQuery.GetNodesByType("RecipeDetails").ToList();

                // sort all recipes as per sortvalue
                if (recipeList != null && recipeList.Count > 0)
                {
                    if (sortValue.Equals("ASC"))
                    {
                        recipeList = recipeList.OrderBy(x => x.Name).ToList();
                    }
                    else
                    {
                        recipeList = recipeList.OrderByDescending(x => x.Name).ToList();
                    }
                }

                // filter all receipes as per filtervalue
                if (cuisineTypeFilter != null && cuisineTypeFilter.Count > 0)
                    recipeList = recipeList.Where(r => r.GetProperty("cuisineType").Value.Split(',').Any(code => cuisineTypeFilter.Contains(code))).ToList();

                if (productFilter != null && productFilter.Count > 0)
                    recipeList = recipeList.Where(r => r.GetProperty("product").Value.Split(',').Any(code => productFilter.Contains(code))).ToList();

                if (cookingTimeFilter != null && cookingTimeFilter.Count > 0)
                    recipeList = recipeList.Where(r => ValidateCookingtime(cookingTimeFilter, r.GetProperty("cookingTimeInMinutes").Value)).ToList();

                if (difficultyLevelFilter != null && difficultyLevelFilter.Count > 0)
                    recipeList = recipeList.Where(r => ValidateDifficulty(difficultyLevelFilter, r.GetProperty("difficultyLevel").Value)).ToList();

                // take count of filtered receipes
                int recipeCount = recipeList.Count;

                // skip previously shown recipes & take only required number of receipes e.g. 4 receipes
                recipeList = recipeList.Skip(startIndex).Take(itemCount).ToList();

                // add recipes to list
                if (recipeList != null && recipeList.Count > 0)
                {
                    foreach (var recipeItem in recipeList)
                    {
                        var allImages = recipeItem.GetProperty("images").Value.Split(',');
                        var imageUrl = SandhurstWebSite.Helper.Common.GetImageUrl(allImages[0]);

                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = "http://placehold.it/300x400&text=No+Image";
                        }

                        recipes.Add(new Recipe
                        {
                            RecipeTitle = recipeItem.Name,
                            RecipeImage = imageUrl,
                            RecipeLink = recipeItem.NiceUrl,
                            RecipeCount = recipeCount
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, ex.StackTrace);
            }

            return Json(recipes);
        }

        /// <summary>
        /// logic to validate recipe cooking time matched to filter value or not
        /// </summary>
        /// <param name="filterValue">value of selected filter</param>
        /// <param name="cookingTimeValue">recipe cooking time</param>
        /// <returns>true/false</returns>
        private bool ValidateCookingtime(List<string> filterValue, string cookingTimeValue)
        {
            int timeToCook = Convert.ToInt32(cookingTimeValue);

            // check if cooking time exist in filter & if yes then compare it
            if (filterValue.Contains("15min") && timeToCook == 15) return true;
            else if (filterValue.Contains("30min") && timeToCook == 30) return true;
            else if (filterValue.Contains("45min") && timeToCook == 45) return true;
            else if (filterValue.Contains("60min") && timeToCook == 60) return true;
            else if (filterValue.Contains("OverAnHour") && timeToCook > 60) return true;
            else return false;
        }

        /// <summary>
        /// logic to validate recipe difficulty matched to filter value or not
        /// </summary>
        /// <param name="filterValue">value of selected filter</param>
        /// <param name="difficultyValue">recipe difficulty</param>
        /// <returns></returns>
        private bool ValidateDifficulty(List<string> filterValue, string difficultyValue)
        {
            // check if recipe difficulty value exist in filter & if yes then compare it
            if (filterValue.Contains("Low") && difficultyValue == "Low") return true;
            else if (filterValue.Contains("Medium") && difficultyValue == "Medium") return true;
            else if (filterValue.Contains("High") && difficultyValue == "High") return true;
            else return false;
        }
    }
}
