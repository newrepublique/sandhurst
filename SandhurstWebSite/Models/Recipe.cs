﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SandhurstWebSite.Models
{
    public class Recipe
    {
        public string RecipeTitle { get; set; }
        public string RecipeImage { get; set; }
        public string RecipeLink { get; set; }
        public int RecipeCount { get; set; }
    }

    public class Ingredient
    {
        public string IngredientTitle { get; set; }
        public string IngredientQty { get; set; }
    }
}