﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SandhurstWebSite.Models
{   
    public class Validation
    {
        public string ContentTypeAlias { get; set; }
        public string PropertyAlias { get; set; }
        public string Limit { get; set; }
    }

    [XmlRootAttribute("RichTextValidations")]
    public class RichTextValidations
    {
        [XmlElement("Validation")]
        public List<Validation> validation { get; set; }
    }
}