﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SandhurstWebSite.Models
{
    public class Distributors
    {
        public int PkDistributorId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Location { get; set; }
        public string LocationFullName { get; set; }
    }
}