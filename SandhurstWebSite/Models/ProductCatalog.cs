﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SandhurstWebSite.Models
{
    /// <summary>
    /// Product Catalog
    /// </summary>
    public class ProductCatalog
    {
        public string catalogName { get; set; }
        public string catalogDescription { get; set; }
        public string catalogImagePath { get; set; }
        public string catalogFilePath { get; set; }
    }
}