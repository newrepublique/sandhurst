﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SandhurstWebSite.Models
{
    /// <summary>
    /// Contact Us Enquiry Form
    /// </summary>
    public class ContactUsEnqueryForm
    {
        public string strEnquryType { get; set; }
        public string strname { get; set; }
        public string strEmail { get; set; }
        public string strPhone { get; set; }
        public string strCustomerType { get; set; }
        public string strEnquiryDetails { get; set; }

        public string recaptcha_response_field { get; set; }
        public string recaptcha_challenge_field { get; set; }


    }
}