﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SandhurstWebSite.Models
{
    public class Newsletter
    {
        public string NewsletterTitle { get; set; }
        public string NewsletterDescription { get; set; }
        public string NewsletterFile { get; set; }
    }
}