﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SandhurstWebSite.Models
{
    public class Search
    {
        public string ResultName { get; set; }
        public string ResultType { get; set; }
        public string ResultDescription { get; set; }
        public string ResultUrl { get; set; }
        public int ResultCount { get; set; }
    }
}