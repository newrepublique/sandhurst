﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SandhurstWebSite.Models
{
    public class Product
    {
        public string ProductTitle { get; set; }
        public string ProductImage { get; set; }
        public string ProductSizeQty { get; set; }
        public string ProductLink { get; set; }
        public int ProductCount { get; set; }
    }
}