﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SandhurstWebSite.Models
{
    public class ImageSlide
    {
        public string TypeOfSlide { get; set; }
        public string TitleOfSlide { get; set; }
        public string DescriptionOfSlide { get; set; }
        public string CTAButtonLink { get; set; }
        public string CTAButtonLinkNewWindow { get; set; }
        public string BackgroundImageOfSlider { get; set; }
    }
}