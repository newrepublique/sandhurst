﻿using SandhurstWebSite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.Serialization;
using umbraco;
using umbraco.BasePages;
using umbraco.interfaces;
using umbraco.NodeFactory;
using Umbraco.Core.Events;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using uComponents.DataTypes.UrlPicker;
using uComponents.DataTypes.UrlPicker.Dto;

namespace Site.EventHandlers
{
    /// <summary>
    /// Class used to handle the Umbraco application events
    /// </summary>
    public class StartupEventHandlers : Umbraco.Core.ApplicationEventHandler
    {

        public StartupEventHandlers()
        {
            ContentService.Saving += ContentService_Saving;
        }

        void ContentService_Saving(IContentService sender, SaveEventArgs<IContent> e)
        {
            try
            {


                #region Rich Text Field Validations
                string strErrorField = string.Empty;
                string strErrorFieldLimit = string.Empty;
                if (!ValidateRichTextEditorLimit(e, out strErrorField, out strErrorFieldLimit))
                {
                    e.Cancel = true;
                    e.CanCancel = true;
                    //Umbraco.Web.UI.Pages.ClientTools.Scripts.CloseModalWindow();
                    //Umbraco.Web.UI.Pages.ClientTools.Scripts.OpenModalWindow("/UmbracoCustomError?errorField=" + strErrorField + "&errorFieldLimit=" + strErrorFieldLimit, "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                    //Umbraco.Web.UmbracoContext.Current.UmbracoUser. UmbracoEnsuredPage.Current.speechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Character limit exceeded!");
                    //var tools = BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.success, "ATENTION !!!", "WE NEED YOU!");
                    //tools.RefreshTree();

                    BasePage.Current.ClientTools.CloseModalWindow();
                    BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?errorField=" + strErrorField + "&errorFieldLimit=" + strErrorFieldLimit, "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                    BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Character limit exceeded!");

                }
                #endregion

                #region Menu Item Display Index validation
                if (!ValidateMenuItemItemsDisplayIndex(e))
                {
                    e.Cancel = true;
                    e.CanCancel = true;
                    BasePage.Current.ClientTools.CloseModalWindow();
                    BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?error=MenuItemIndexNotAllowed", "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                    BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Specified 'Menu Item Index' already assigned to another panel!");
                    e.Cancel = true;
                }
                #endregion


                #region Dynamic Panels Display Index validation
                if (!ValidateDynamicPanelsDisplayIndex(e))
                {
                    e.Cancel = true;
                    e.CanCancel = true;
                    BasePage.Current.ClientTools.CloseModalWindow();
                    BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?error=IndexNotAllowed", "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                    BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Specified 'Display Index' already assigned to another panel!");
                    e.Cancel = true;
                }
                #endregion

                #region Validate Step count, should be 2

                foreach (Content obj in e.SavedEntities)
                {
                    string steps = string.Empty;
                    string strFieldName = string.Empty;
                    if (obj.ContentType.Alias.Equals("RecipeDetails", StringComparison.OrdinalIgnoreCase))
                    {
                        steps = obj.GetValue<string>("methodDescription");
                        strFieldName = "methodDescription";
                    }
                    else if (obj.ContentType.Alias.Equals("StepsPanel", StringComparison.OrdinalIgnoreCase))
                    {
                        steps = obj.GetValue<string>("steps");
                    }
                    if (!string.IsNullOrEmpty(steps))
                    {
                        XElement xmlTree = XElement.Parse(steps);


                        if (xmlTree.Elements().Count() < 2)
                        {
                            BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?error=Min2Steps", "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                            BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Minimum 2 steps are required!");
                            e.Cancel = true;
                        }

                        int stepCount = 0;
                        foreach (XElement xle in xmlTree.Elements())
                        {
                            stepCount++;
                            if (CountCharFromHTML(xle.Value) > 300)
                            {
                                strErrorField = strFieldName + "Step" + stepCount;
                                strErrorFieldLimit = "300";
                                e.Cancel = true;
                                e.CanCancel = true;

                                BasePage.Current.ClientTools.CloseModalWindow();
                                BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?errorField=" + strErrorField + "&errorFieldLimit=" + strErrorFieldLimit, "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                                BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Character limit exceeded!");
                            }
                        }
                    }
                }

                #endregion

                #region Featured Range Validations
                /*This validation is done to fulfill following requirements                 
                 1. Other navigation items/ menu items should not link to 'Range'
                 2. Featured Ranage should link to Range only and not to other pages/ nodes of the web site
                 3. Content author can only choose 3 ranges as fetured ranges which will be shown in 'OUR RANGE' dropdown within header and footer                                 
                 */

                foreach (Content obj in e.SavedEntities)
                {
                    if (obj.ContentType.Alias.Equals("MenuItem", StringComparison.OrdinalIgnoreCase))
                    {
                        //get list of all ranges node ids
                        var ranges = uQuery.GetNodesByType("ProductRange").ToList().Select(x => x.Id).ToArray();
                        var linkTo = obj.GetValue("linkTo") != null ? obj.GetValue("linkTo") : 0;
                        if (ranges.Contains(Convert.ToInt32(linkTo)))
                        {
                            BasePage.Current.ClientTools.CloseModalWindow();
                            BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?error=RangeNotAllowed", "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                            BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Link to Product Ranges is not allowed!");
                            e.Cancel = true;
                            break;
                        }
                    }

                    if (obj.ContentType.Alias.Equals("MenuItemProductRange", StringComparison.OrdinalIgnoreCase))
                    {
                        //validate that all Range Navigation nodes should only contain link to Ranges
                        var totalRanges = uQuery.GetNodesByType("ProductRange").ToList().Select(x => x.Id).ToArray();
                        var linkTo1 = obj.GetValue("linkTo") != null ? obj.GetValue("linkTo") : 0;
                        if (!totalRanges.Contains(Convert.ToInt32(linkTo1)))
                        {

                            BasePage.Current.ClientTools.CloseModalWindow();
                            BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?error=OnlyRangeAllowed", "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                            BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Only Link to 'Product Ranges' is allowed!");
                            e.Cancel = true;
                            break;
                        }

                        //get list of all ranges node ids
                        var rangeMenuItems = uQuery.GetNodesByType("MenuItemProductRange").ToList().Select(x => x.Id).ToArray();
                        if (rangeMenuItems.Count() >= 3 && (!rangeMenuItems.Contains(obj.Id)))
                        {

                            BasePage.Current.ClientTools.CloseModalWindow();
                            BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?error=FeaturedRagneLimitExceeded", "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                            BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Only 3 'Product Ranges' can be selected as 'Featured Range'!");
                            e.Cancel = true;
                            break;
                        }
                    }
                }

                #endregion

                #region Home page Image Slider Link Validation
                foreach (Content obj in e.SavedEntities)
                {
                    if (obj.ContentType.Alias.Equals("Home", StringComparison.OrdinalIgnoreCase))
                    {
                        for (int i = 1; i <= 5; i++)
                        {
                            string typeOfSlide = !string.IsNullOrEmpty(obj.GetValue<string>("typeOfSlide" + i))
                                                ? umbraco.library.GetPreValueAsString(Convert.ToInt32(obj.GetValue<string>("typeOfSlide" + i)))
                                                : string.Empty;
                            UrlPickerState linkTo = UrlPickerState.Deserialize(obj.GetValue<string>("cTAButtonLink" + i));

                            //If type of slide is selected and link to is selected
                            if (!string.IsNullOrEmpty(typeOfSlide) && linkTo.Mode == UrlPickerMode.Content && linkTo.NodeId != null && linkTo.NodeId > 0)
                            {
                                if (typeOfSlide.Contains("Recipe"))
                                {
                                    //get list of all recipes node ids
                                    var recipes = uQuery.GetNodesByType("RecipeDetails").ToList().Select(x => x.Id).ToArray();

                                    if (!recipes.Contains(linkTo.NodeId.Value))
                                    {
                                        BasePage.Current.ClientTools.CloseModalWindow();
                                        BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?error=OnlyRecipeAllowed&FieldName=CTA Button Link" + i.ToString() + "&TabName=Image Slider " + i.ToString(), "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                                        BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Only recipe allowed!");
                                        e.Cancel = true;
                                    }
                                }
                                else if (typeOfSlide.Contains("Product"))
                                {
                                    //get list of all products node ids
                                    var product = uQuery.GetNodesByType("Product").ToList().Select(x => x.Id).ToArray();

                                    if (!product.Contains(linkTo.NodeId.Value))
                                    {
                                        BasePage.Current.ClientTools.CloseModalWindow();
                                        BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?error=OnlyProductAllowed&FieldName=CTA Button Link" + i.ToString() + "&TabName=Image Slider " + i.ToString(), "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                                        BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Only product allowed!");
                                        e.Cancel = true;
                                    }
                                }
                            }
                            //If type of slide is not selected and link to is selected
                            else if (string.IsNullOrEmpty(typeOfSlide) && linkTo.Mode == UrlPickerMode.Content && linkTo.NodeId != null && linkTo.NodeId > 0)
                            {
                                //get list of all recipes node ids
                                var recipes = uQuery.GetNodesByType("RecipeDetails").ToList().Select(x => x.Id).ToArray();

                                if (!recipes.Contains(linkTo.NodeId.Value))
                                {
                                    //get list of all products node ids
                                    var product = uQuery.GetNodesByType("Product").ToList().Select(x => x.Id).ToArray();

                                    if (!product.Contains(linkTo.NodeId.Value))
                                    {
                                        BasePage.Current.ClientTools.CloseModalWindow();
                                        BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?error=OnlyRecipeProductAllowed&FieldName=CTA Button Link" + i.ToString() + "&TabName=Image Slider " + i.ToString(), "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                                        BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Only product/recipe allowed!");
                                        e.Cancel = true;
                                    }
                                }
                            }
                        }
                    }
                }

                #endregion

                #region Link/ node picker Validation
                if (!ValidateContentpicker(e)) { return; }
                #endregion
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Exeception occurend in ContentService_Publishing " + ex.StackTrace);
            }
            return;
        }


        #region Menu Items(nodes) Display Index validation method
        /// <summary>
        /// Check siblings for same Navigation Index
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private bool ValidateMenuItemItemsDisplayIndex(SaveEventArgs<IContent> e)
        {
            foreach (Content content in e.SavedEntities)
            {
                //Get parent Node and its childrens to get all panels
                if ((content.ContentType.Alias.Equals("MenuItem", StringComparison.OrdinalIgnoreCase) && content.ParentId != 0) || (content.ContentType.Alias.Equals("MenuItemMenuItemProductRange", StringComparison.OrdinalIgnoreCase) && content.ParentId != 0))
                {
                    Node parentNode = new Node(content.ParentId);
                    if (parentNode != null)
                    {
                        string currentDisplayIndex = content.HasProperty("topNavigationIndex") ? content.GetValue<string>("topNavigationIndex") : string.Empty;
                        if (!string.IsNullOrEmpty(currentDisplayIndex))
                        {
                            List<INode> lstSiblings = parentNode.ChildrenAsList;
                            foreach (var sibling in lstSiblings)
                            {
                                //skip current saving node
                                if (sibling.Id == content.Id)
                                {
                                    continue;
                                }
                                string siblingsDispalyIndex = sibling.HasProperty("topNavigationIndex") ? sibling.GetProperty<string>("topNavigationIndex") : string.Empty;
                                if (!string.IsNullOrEmpty(siblingsDispalyIndex) && string.Equals(currentDisplayIndex, siblingsDispalyIndex))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
                else if (content.ContentType.Alias.Equals("MenuItemMaster", StringComparison.OrdinalIgnoreCase) && content.ParentId != 0)
                {
                    //Also Check for Menu Item Master
                    Node parentNode = new Node(content.ParentId);
                    if (parentNode != null)
                    {
                        string currentDisplayIndex = content.HasProperty("menuItemIndex") ? content.GetValue<string>("menuItemIndex") : string.Empty;
                        if (!string.IsNullOrEmpty(currentDisplayIndex))
                        {
                            List<INode> lstSiblings = parentNode.ChildrenAsList;
                            foreach (var sibling in lstSiblings)
                            {
                                //skip current saving node
                                if (sibling.Id == content.Id)
                                {
                                    continue;
                                }
                                string siblingsDispalyIndex = sibling.HasProperty("menuItemIndex") ? sibling.GetProperty<string>("menuItemIndex") : string.Empty;
                                if (!string.IsNullOrEmpty(siblingsDispalyIndex) && string.Equals(currentDisplayIndex, siblingsDispalyIndex))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }
        #endregion


        #region Dynamic Panels Display Index validation method
        /// <summary>
        /// Check siblings for same Display Index
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private bool ValidateDynamicPanelsDisplayIndex(SaveEventArgs<IContent> e)
        {
            foreach (Content content in e.SavedEntities)
            {
                //Get parent Node and its childrens to get all panels
                if (content.ContentType.Alias.Contains("Panel") && content.ParentId != null)
                {
                    Node parentNode = new Node(content.ParentId);
                    if (parentNode != null && parentNode.NodeTypeAlias == "CustomContentPage")
                    {
                        string currentDisplayIndex = content.HasProperty("displayIndex") ? content.GetValue<string>("displayIndex") : string.Empty;
                        if (!string.IsNullOrEmpty(currentDisplayIndex))
                        {
                            List<INode> lstSiblings = parentNode.ChildrenAsList;
                            foreach (var sibling in lstSiblings)
                            {
                                //skip current saving node
                                if (sibling.Id == content.Id)
                                {
                                    continue;
                                }
                                string siblingsDispalyIndex = sibling.HasProperty("displayIndex") ? sibling.GetProperty<string>("displayIndex") : string.Empty;
                                if (!string.IsNullOrEmpty(siblingsDispalyIndex) && string.Equals(currentDisplayIndex, siblingsDispalyIndex))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }
        #endregion

        #region Rich Text Editor
        /// <summary>
        /// Validate Rich Text Editor Limit set to \App_Data\RichTextValidation.xml
        /// </summary>
        /// <param name="e"></param>
        /// <param name="strErrorField"></param>
        /// <param name="strErrorFieldLimit"></param>
        /// <returns></returns>
        private bool ValidateRichTextEditorLimit(SaveEventArgs<IContent> e, out string strErrorField, out string strErrorFieldLimit)
        {
            strErrorField = string.Empty;
            strErrorFieldLimit = string.Empty;

            #region GetValidationDetails
            object obj1 = System.Web.HttpRuntime.Cache.Get("richTextValidations");
            RichTextValidations lstCachedRichTextEditors = obj1 as RichTextValidations;

            if (lstCachedRichTextEditors == null || lstCachedRichTextEditors.validation.Count <= 0)
            {
                using (TextReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(@"~\App_Data\RichTextValidation.xml")))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(RichTextValidations));
                    lstCachedRichTextEditors = (RichTextValidations)serializer.Deserialize(reader);

                    System.Web.HttpRuntime.Cache.Insert("richTextValidations", lstCachedRichTextEditors, new System.Web.Caching.CacheDependency(System.Web.HttpContext.Current.Server.MapPath(@"~\App_Data\RichTextValidation.xml")));
                }
            }
            #endregion

            if (lstCachedRichTextEditors != null && lstCachedRichTextEditors.validation.Count > 0)
            {
                foreach (Content content in e.SavedEntities)
                {
                    if (content.HasProperty("youMayLikeDescription") && !string.IsNullOrEmpty(content.GetValue<string>("youMayLikeDescription")))
                    {
                        Validation validation = lstCachedRichTextEditors.validation.Find(obj => obj.PropertyAlias == "youMayLikeDescription");

                        if (Convert.ToInt32(validation.Limit) < CountCharFromHTML(content.GetValue<string>("youMayLikeDescription")))
                        {
                            strErrorField = validation.PropertyAlias;
                            strErrorFieldLimit = validation.Limit;
                            return false;
                        }

                    }

                    List<Validation> lstValidation = lstCachedRichTextEditors.validation.FindAll(obj => obj.ContentTypeAlias == content.ContentType.Alias);
                    if (lstValidation != null && lstValidation.Count > 0)
                    {
                        foreach (Validation validation in lstValidation)
                        {
                            if (content.HasProperty(validation.PropertyAlias) && Convert.ToInt32(validation.Limit) < CountCharFromHTML(content.GetValue<string>(validation.PropertyAlias)))
                            {

                                strErrorField = validation.PropertyAlias;
                                strErrorFieldLimit = validation.Limit;
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Strip the html tag from input string and returns no of charactors
        /// </summary>
        /// <param name="HTML"></param>
        /// <returns></returns>
        private int CountCharFromHTML(string HTML)
        {
            if (!string.IsNullOrEmpty(HTML))
            {

                return Regex.Replace(HTML, @"<(.|\n)*?>|\n|\r", string.Empty).Length;
            }
            return 0;
        }
        #endregion

        #region Content Picker
        /// <summary>
        /// Validate node selected by content picker
        /// </summary>
        /// <param name="e">currently saving/publishing content</param>
        /// <returns>true/false</returns>
        private bool ValidateContentpicker(SaveEventArgs<IContent> e)
        {
            bool isValidNode = true;
            try
            {
                // list of property alias having datatype content picker
                string[] pickerproperties = new string[] { "youMayLikeActionLinkUrl", "linkTo", "action1Link", "action2Link", "actionLinkUrl" };

                foreach (Content obj in e.SavedEntities)
                {
                    // check if current content has respective property
                    if (pickerproperties.Any(o => obj.HasProperty(o)))
                    {
                        //Get node for property value
                        var propertyAlias = pickerproperties.Where(o => obj.HasProperty(o)).Select(o => o);
                        if (propertyAlias != null && propertyAlias.Count() > 0)
                        {
                            foreach (var strProperty in propertyAlias)
                            {
                                if (!string.IsNullOrEmpty(strProperty) && !string.IsNullOrEmpty(obj.GetValue<string>(strProperty)))
                                {
                                    Node node = new Node(Convert.ToInt32(obj.GetValue(strProperty)));

                                    //check node has template (means the UI page) && if not then show error
                                    if (node.template <= 0 || node.NodeTypeAlias == "Search")
                                    {
                                        BasePage.Current.ClientTools.CloseModalWindow();
                                        BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?error=NodeNotAllowed&FieldName=" + strProperty, "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                                        BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "Link to this page is not allowed!");
                                        e.Cancel = true;
                                        isValidNode = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Exeception occurend in ContentService_Publishing ValidateContentpicker" + ex.StackTrace);
            }
            return isValidNode;
        }
        #endregion
    }

}